package org.eclipse.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.eclipse.model.Client;


/**
 * Servlet implementation class InscriptionServlet
 */
@WebServlet("/creationClient")
public class CreationClientServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "/WEB-INF/creerClient.jsp";
		this.getServletContext().getRequestDispatcher(url).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String message = null;
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String tel = request.getParameter("tel");
		String email = request.getParameter("email");
		Client client = new Client();
		client.setNom(nom);
		client.setPrenom(prenom);
		client.setTel(tel);
		client.setEmail(email);
		
		if (nom.trim().isEmpty() || prenom.trim().isEmpty() || tel.trim().isEmpty() || email.isEmpty() ) {
			message = "Erreur - Vous n'avez pas rempli tous les champs obligatoires. <br> <a href=\"creerAdresse.jsp\">Cliquez ici</a> pour accéder au formulaire de création d'une commande.";
		} else {
			 message = "Commande créée avec succès !";
		}
		session.setAttribute("client", client);
		request.setAttribute("message", message);
		String url = "/WEB-INF/afficherClient.jsp";
		this.getServletContext().getRequestDispatcher(url).forward(request, response);
	}

}
