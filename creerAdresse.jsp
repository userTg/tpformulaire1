<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>adresse</title>
</head>
<body>
	<form method="post" action="creationAdresse">
		<div>Formulaire de création d'une adresse</div>
		<div>
			<label for="rue"> Rue * :</label> <input type="text" id="rue"
				name="rue" value="" />
		</div>
		<div>
			<label for="codePostal"> Code postal * :</label> <input type="text"
				id="codePostal" name="codePostal" value="" />
		</div>
		<div>
			<label for="ville"> Ville * :</label> <input type="text"
				id="ville" name="ville" value="" />
		</div>
		<input type="submit" value="Enregistrer" />
	</form>
</body>
</html>